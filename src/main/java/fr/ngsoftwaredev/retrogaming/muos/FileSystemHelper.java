package fr.ngsoftwaredev.retrogaming.muos;

import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;

@Component
public class FileSystemHelper {

    public List<Path> listChildrenDirectories(final Path p) {
        File root = p.toFile();
        if (!root.isDirectory()) {
            return emptyList();
        }

        return Optional.ofNullable(root.listFiles(File::isDirectory)).map(List::of)
                .orElse(emptyList())
                .stream()
                .map(f -> FileSystems.getDefault().getPath(f.getAbsolutePath()))
                .collect(Collectors.toList());
    }

}
