package fr.ngsoftwaredev.retrogaming.muos;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.ngsoftwaredev.retrogaming.muos.config.MuOSConfig;
import fr.ngsoftwaredev.retrogaming.muos.config.SkraperConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.nio.file.FileVisitResult.CONTINUE;

@Service
@Slf4j
public class MuOSCatalogMakerService {

    private static final String catalogNameFormat = "muos-catalogue-%s";
    private static final String catalogZipFormat = catalogNameFormat + ".zip";
    private static final String tmpCatalogDirFormat = "." + catalogNameFormat;

    private final ObjectMapper objectMapper;

    private final MuOSCoreInfoService coreInfoService;
    private final FileSystemHelper filesystemHelper;
    private final GamelistHelper gamelistHelper;

    private final MuOSConfig muOSConfig;
    private final SkraperConfig skraperConfig;

    public MuOSCatalogMakerService(
            @Autowired ObjectMapper objectMapper,
            @Autowired MuOSCoreInfoService coreInfoService,
            @Autowired FileSystemHelper filesystemHelper,
            @Autowired GamelistHelper gamelistHelper,
            @Autowired MuOSConfig muOSConfig,
            @Autowired SkraperConfig skraperConfig) {
        this.objectMapper = objectMapper;
        this.coreInfoService = coreInfoService;
        this.filesystemHelper = filesystemHelper;
        this.gamelistHelper = gamelistHelper;
        this.muOSConfig = muOSConfig;
        this.skraperConfig = skraperConfig;
    }

    public void makeCatalog(final Path outputFolderPath) throws IOException {
        final Map<String, String> systemNamesPerContentRootDir = coreInfoService.scanCoreInfo();
        log.info(
                "System names per content root dir {}",
                objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(systemNamesPerContentRootDir));


        final long timestamp = System.currentTimeMillis();
        Path tmpCatalogDir = Files.createTempDirectory(String.format(tmpCatalogDirFormat, timestamp));

        try {
            MuOSCatalogue catalogue = new MuOSCatalogue(tmpCatalogDir);

            Path romsRootPath = FileSystems.getDefault().getPath(muOSConfig.getSdCardMountPath(), MuOSConfig.Path.roms.getPath());
            log.info("Will scan for Skraper media and metadata in {}", romsRootPath);
            for (Path romFolder : filesystemHelper.listChildrenDirectories(romsRootPath)) {
                log.info("Processing rom content dir {}", romFolder);
                final String romFolderName = romFolder.toFile().getName();

                Optional<String> optSystemName = Optional.ofNullable(systemNamesPerContentRootDir.get(romFolderName));
                if (optSystemName.isEmpty()) {
                    log.warn("No core mapping found for {}", romFolderName);
                    continue;
                }

                final String systemName = optSystemName.get();

                // Copy art files
                for (String kind : List.of(skraperConfig.getBoxArtDirName(), skraperConfig.getPreviewArtDirName())) {
                    Path artFolder = romFolder.resolve(skraperConfig.getMediaRootDirName()).resolve(kind);
                    if (Files.notExists(artFolder)) {
                        log.warn("Missing art folder {}", artFolder);
                        continue;
                    }
                    Files.walkFileTree(artFolder, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                            if (file.toFile().getName().endsWith(skraperConfig.getArtFileExtension())) {
                                catalogue.addMedia(file, systemName, kind);
                            }
                            return CONTINUE;
                        }
                    });
                }

                // Extract texts
                Path gamelistPath = romFolder.resolve(skraperConfig.getGamelistName());
                if (Files.exists(gamelistPath)) {
                    gamelistHelper.extractTexts(catalogue, systemName, gamelistPath);
                } else {
                    log.warn("Missing gamelist {}", gamelistPath);
                }
            }

            Path zippedCatalogue = outputFolderPath.resolve(String.format(catalogZipFormat, timestamp));
            log.info("Creating catalogue archive...");
            ZipHelper.zipDirectoryTo(catalogue.getCatalogRootForArchive(), zippedCatalogue);
            log.info("Wrote catalogue archive to {}", zippedCatalogue);

        } finally {
            FileSystemUtils.deleteRecursively(tmpCatalogDir);
            log.info("Deleted temporary catalogue {}", tmpCatalogDir);
        }
    }


}
