package fr.ngsoftwaredev.retrogaming.muos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipHelper {

    public static void zipDirectoryTo(Path sourceDir, Path archive) throws IOException {
        File dir = sourceDir.toFile();
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(archive.toFile()));
        try {
            zipFile(dir, dir.getName(), zos);
        } finally {
            zos.close();
        }
    }

    private static void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
        if (fileToZip.isHidden()) {
            return;
        }
        if (fileToZip.isDirectory()) {
            if (fileName.endsWith(File.separator)) {
                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.closeEntry();
            } else {
                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                zipOut.closeEntry();
            }
            for (File childFile : Optional.ofNullable(fileToZip.listFiles()).map(List::of).orElse(List.of())) {
                zipFile(
                        childFile,
                        String.join(File.separator, fileName, childFile.getName()),
                        zipOut);
            }
            return;
        }
        FileInputStream fis = new FileInputStream(fileToZip);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
        fis.close();
    }
}
