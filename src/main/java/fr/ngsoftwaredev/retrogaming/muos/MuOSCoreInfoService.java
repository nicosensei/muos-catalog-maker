package fr.ngsoftwaredev.retrogaming.muos;

import fr.ngsoftwaredev.retrogaming.muos.config.MuOSConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class MuOSCoreInfoService {

    private final Path rootPath;
    private final String coreInfoFilename;
    private final FileSystemHelper filesystemHelper;

    public MuOSCoreInfoService(
            @Autowired MuOSConfig config,
            @Autowired FileSystemHelper filesystemHelper) {
        this.rootPath = FileSystems.getDefault().getPath(config.getSdCardMountPath(), MuOSConfig.Path.infoCore.getPath());
        this.coreInfoFilename = config.getCoreInfoFileName();
        this.filesystemHelper = filesystemHelper;
        log.info("Core info is located in {}", rootPath);

    }

    public Map<String, String> scanCoreInfo() throws IOException {
        Map<String, String> systemNamesPerContentRootDir = new HashMap<>();
        for (Path contentRoot : filesystemHelper.listChildrenDirectories(rootPath)) {
            Path coreCfg = contentRoot.resolve(coreInfoFilename);
            if (!Files.exists(coreCfg)) {
                log.warn("Missing core info file {}", coreCfg);
                continue;
            }
            log.info("Found core info file {}", coreCfg);
            systemNamesPerContentRootDir.put(contentRoot.toFile().getName(), Files.readAllLines(coreCfg).get(1));
        }
        return systemNamesPerContentRootDir;
    }

}
