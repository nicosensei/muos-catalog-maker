package fr.ngsoftwaredev.retrogaming.muos;

import fr.ngsoftwaredev.retrogaming.muos.config.MuOSConfig;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.function.BiConsumer;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Slf4j
@Value
public class MuOSCatalogue {

    private final static String MNT = "mnt";
    private final static String MMC = "mmc";
    private final static String TEXT = "text";
    private final static String textFileNameFormat = "%s.txt";


    private final Path rootPath;
    private final Path catalogPath;

    public MuOSCatalogue(Path rootPath) throws IOException {
        this.rootPath = rootPath;
        this.catalogPath = Files.createDirectories(
                rootPath.resolve(MNT).resolve(MMC).resolve(MuOSConfig.Path.infoCatalog.getPath()));
        log.info("Initialized catalog in {}", catalogPath);
    }

    public void addMedia(Path sourceFile, String system, String kind) throws IOException {
        addResource(
                sourceFile,
                system,
                kind,
                sourceFile.toFile().getName(),
                (src, dst) -> {
                    try {
                        Files.copy(src, dst, REPLACE_EXISTING);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    public void addText(String system, String game, String text) throws IOException {
        addResource(
                text,
                system,
                TEXT,
                String.format(textFileNameFormat, game),
                (txt, dst) -> {
                    try {
                        Files.writeString(dst, text, StandardOpenOption.CREATE);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    public Path getCatalogRootForArchive() {
        return rootPath.resolve(MNT);
    }

    private <T> void addResource(
            T source,
            String system,
            String kind,
            String fileName,
            BiConsumer<T, Path> sourceConsumer) throws IOException {
        Path dir = catalogPath.resolve(system).resolve(kind);
        if (Files.notExists(dir)) {
            dir = Files.createDirectories(dir);
        }

        final Path resourcePath = dir.resolve(fileName);
        sourceConsumer.accept(source, resourcePath);
        if (log.isDebugEnabled()) {
            log.debug("Added {}", resourcePath);
        }
    }

}
