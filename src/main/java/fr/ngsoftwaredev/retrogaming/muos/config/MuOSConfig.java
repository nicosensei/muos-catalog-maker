package fr.ngsoftwaredev.retrogaming.muos.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "muos")
@Data
public class MuOSConfig {

    @AllArgsConstructor
    public enum Path {
        infoCore("MUOS/info/core"),
        infoCatalog("MUOS/info/catalogue"),
        roms("ROMS");

        @Getter
        private final String path;
    }

    @NonNull
    private String sdCardMountPath = "";

    @NonNull
    private String coreInfoFileName = "";

}
