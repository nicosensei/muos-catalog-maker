package fr.ngsoftwaredev.retrogaming.muos.config;

import lombok.Data;
import lombok.NonNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "skraper")
@Data
public class SkraperConfig {

    @NonNull
    private String mediaRootDirName = "";

    @NonNull
    private String boxArtDirName = "";

    @NonNull
    private String previewArtDirName = "";

    @NonNull
    private String foldersArtDirName = "";

    @NonNull
    private String gamelistName = "";

    @NonNull
    private String artFileExtension = "";

}
