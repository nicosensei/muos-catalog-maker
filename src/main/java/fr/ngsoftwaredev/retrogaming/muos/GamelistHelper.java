package fr.ngsoftwaredev.retrogaming.muos;

import com.arakelian.jq.ImmutableJqLibrary;
import com.arakelian.jq.ImmutableJqRequest;
import com.arakelian.jq.JqLibrary;
import com.arakelian.jq.JqResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Builder;
import lombok.extern.jackson.Jacksonized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@Slf4j
public class GamelistHelper {

    @Builder
    @Jacksonized
    private record SparseGame(String path, String text) {}

    private static final TypeReference<Map<String, Object>> JSON = new TypeReference<>() {};
    private static final TypeReference<List<SparseGame>> SPARSE_GAME_LIST = new TypeReference<>() {};
    private static final String jqFilter = "[.game[] | { \"path\": .path, \"text\": .desc }]";

    private final XmlMapper xmlMapper = new XmlMapper();
    private final JqLibrary jq = ImmutableJqLibrary.of();
    private final ObjectMapper objectMapper;

    public GamelistHelper(@Autowired ObjectMapper objectMapper) throws IOException {
        this.objectMapper = objectMapper;
    }

    public void extractTexts(final MuOSCatalogue catalogue, final String system, final Path gamelistPath) throws IOException {
        final JqResponse response = ImmutableJqRequest.builder()
                .lib(jq)
                .input(objectMapper.writeValueAsString(xmlMapper.readValue(Files.newInputStream(gamelistPath), JSON)))
                .filter(jqFilter)
                .build()
                .execute();
        if (response.hasErrors()) {
            log.warn("Failed to extract texts from {}", gamelistPath);
            return;
        }

        for (SparseGame game : objectMapper.readValue(response.getOutput(), SPARSE_GAME_LIST)) {
            Optional.ofNullable(game.text).ifPresent(text -> {
                try {
                    catalogue.addText(system, gameName(game), text);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    private String gameName(SparseGame game) {
        String[] parts = game.path.split("/");
        String lastPart = parts[parts.length - 1];
        return lastPart.substring(0, lastPart.indexOf("."));
    }

}
