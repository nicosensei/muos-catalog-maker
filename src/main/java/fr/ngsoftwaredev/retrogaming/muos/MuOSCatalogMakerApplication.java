package fr.ngsoftwaredev.retrogaming.muos;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.FileSystems;

@SpringBootApplication
@Slf4j
public class MuOSCatalogMakerApplication implements CommandLineRunner {

    @Autowired
    private MuOSCatalogMakerService service;

    public static void main(String[] args) {
        SpringApplication.run(MuOSCatalogMakerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (args.length != 1) {
            log.error("Usage: {} <output folder path>", MuOSCatalogMakerApplication.class.getSimpleName());
            System.exit(-1);
        }
        service.makeCatalog(FileSystems.getDefault().getPath(args[0]));
    }

}