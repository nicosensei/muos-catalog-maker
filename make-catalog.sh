#!/bin/sh

#JAVA_OPTS="-Xms256m -Xmx256m -agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=*:5005"
#AVA_OPTS="-Xms256m -Xmx256m"

#LOG_LEVEL=DEBUG
LOG_LEVEL=INFO

java $JAVA_OPTS \
  -DAPPLICATION_NAME=-muos-catalog-maker \
  -DLOG_LEVEL=$LOG_LEVEL \
  -DMUOS_SDCARD_MOUNT_POINT=/run/media/nicolas/ROMS \
  -DMUOS_CORE_INFO_FILE_NAME=core.cfg \
  -DSKRAPER_MEDIA_ROOT_DIR_NAME=media \
  -DSKRAPER_MEDIA_BOX_ART_DIR_NAME=box \
  -DSKRAPER_MEDIA_PREVIEW_ART_DIR_NAME=preview \
  -DSKRAPER_MEDIA_FOLDERS_ART_DIR_NAME=folders \
  -DSKRAPER_IMG_EXT=.png \
  -DSKRAPER_GAMELIST_FILE_NAME=gamelist.xml \
  -jar target/muos-catalog-maker-0.1.0-SNAPSHOT.jar $1
