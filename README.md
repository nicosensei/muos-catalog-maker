# muOS Catalog Maker


`muOS` (https://muos.dev/) is a custom firmware for the RG35XX series of emulation consoles made by Anbernic.

Unlike most emulation frontends, `muOS` offers a lot of flexibility to organize your romset as you like in termsof folder structure, and uses an abstraction called the `catalogue` to store scraped media and metadata independently of the romset structure (https://muos.dev/help/artwork).

There is already a premade _catalogue_ archive for the _Tiny Best Set_ romset (https://github.com/antiKk/muOS-Artwork), but what if you prefer using your own self-curated, carefully hand-picked romset?

This is a utility for generating a ready to install (with _Archive Manager_) _muOS_ catalogue archive:

- given a custom romset copied to your muOS SD card
- given said romset has been scraped already
- given rom dirs have been associated to a core in _muOS_ (i.e. <sdcard>/MUOS/info/core is populated)
- given your muOS sdcard is mounted in your local filesystem

As of now the tool consists of an executable Java JAR and runs only on Linux systems. 

More will follow.